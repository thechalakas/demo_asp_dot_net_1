﻿using System.Web;
using System.Web.Mvc;

namespace demo_asp_dot_net_1
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
