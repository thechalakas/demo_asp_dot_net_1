﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace demo_asp_dot_net_1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "A simple dot net application built by study nildana to demonstrate asp dot net web apps";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "contact us via twitter or facebook page. Find the links to them on the home page";

            return View();
        }
    }
}