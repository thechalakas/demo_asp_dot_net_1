﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(demo_asp_dot_net_1.Startup))]
namespace demo_asp_dot_net_1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
