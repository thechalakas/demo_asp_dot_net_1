﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo_asp_dot_net_1.Models
{
    //we define the student model here

    public class student
    {
        //this is the unique ID automatically set by EF
        public int ID { get; set; }

        //the name of the student
        public string student_name { get; set; }

        //the age of the student
        public int student_age { get; set; }

        //email of the student
        //note that the student has to use this exact same email address when she/he registers on the web app to see her marks
        //since we cannot process login/registration process without email address, we cannot allow student entries
        //to be made to the database unless
        //so this becomes a mandatory field.
        //that is why I am using the Required attribute
        [Required]
        public string student_email_address { get; set; }

        //marks scored by the student
        public string student_marks { get; set; }
    }

    //now need to get the context that will work with the database
    public class studentDbContext : DbContext
    {
        //connection string to be used
        public studentDbContext() : base("studentDbContext")
        {

        }

        //DbSets that are the tables 
        //table for students
        public DbSet<student> student_sets { get; set; }

        //this is more about appearance rather than functionality
        //prevents table names from being pluralized
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }

    //IMPORTANT NOTE : BEFORE ADDING CONTROLLER FOR THIS MODEL, BUILD YOUR SOLUTION
}
